WikiCorpus
=============

**Aviso**. Este proyecto ya tiene su tiempo y revisándolo me doy cuenta de muchas cosas que se podrían refactorizar. Es normal, ahora tengo conocimientos que antes no, ¡y más ideas! Pero también tengo intereses diferentes.
Por ello no creo que tenga más cambios que el commit de este aviso. Por supuesto, si alguiente tiene algún interés en este repositorio o quiere desarrollar algo a partir de él, puede comunicarse conmigo.

----

``WikiCorpus`` es una aplicación para la interfaz de línea de comandos (CLI) que tiene como objetivo la extracción de datos de los proyectos Wikimedia para su posterior análisis.

Esta herramienta está en desarrollo y en estos momentos es parte de un proyecto de investigación de humanidades digitales y estado del patrimonio cultural de Canarias en Wikipedia en español, Wikimedia Commons y Wikidata.

En estos momento su utilidad está centrada en este proyecto, por lo que su uso para analizar los proyectos Wikimedia a partir de otros datos puede no servir, ya que sus métodos están enfocados a lo que necesita este proyecto.

Conforme se vaya definiendo el proyecto se irán publicando más detalles.

Licencia
============

WikiCorpus está disponible bajo la licencia *GNU Lesser General Public License* v3.0 (LGPL 3.0), incluida en este repositorio en el archivo ``COPYING.LESSER`` y notificada en cada archivo del código fuente.
